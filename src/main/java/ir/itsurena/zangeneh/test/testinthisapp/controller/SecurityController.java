package ir.itsurena.zangeneh.test.testinthisapp.controller;

import ir.itsurena.zangeneh.test.testinthisapp.JWTUtil;
import ir.itsurena.zangeneh.test.testinthisapp.Service.impl.UMSService;
import ir.itsurena.zangeneh.test.testinthisapp.config.PermissionHolder;
import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 * @author Saeid Zangeneh
 *
 */
@RestController
@RequestMapping(value = "/api")
@CrossOrigin("http://10.40.0.227:4200")
public class SecurityController {

    @Autowired
    UMSService umsService;


    @GetMapping(value = "/getPermissions")
    public ResponseEntity<Permission> getPermissions(@RequestHeader(value = "Authorization", required = true) String jwt) {

        String withoutBearer = JWTUtil.getJwtFromHeader(jwt);

        if (PermissionHolder.getPermissionByJwtToken(jwt) == null) {
            PermissionHolder.addPermission(withoutBearer,umsService.getPermission(jwt));
        }
        Permission permission = PermissionHolder.getPermissionByJwtToken(withoutBearer);

        return new ResponseEntity(permission, HttpStatus.OK);
    }

    @GetMapping(value = "/secure")
    public String getSecuredResource() {
        return "this is accessable by admin and user permission";
    }



}