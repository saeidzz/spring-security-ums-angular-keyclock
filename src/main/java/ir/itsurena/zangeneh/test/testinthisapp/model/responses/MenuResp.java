package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResp {
    private LoginDetail response;

    public LoginDetail getResponse() {
        return response;
    }

    public void setResponse(LoginDetail response) {
        this.response = response;
    }
}
