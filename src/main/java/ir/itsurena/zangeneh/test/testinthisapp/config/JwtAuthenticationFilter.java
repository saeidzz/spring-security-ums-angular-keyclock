package ir.itsurena.zangeneh.test.testinthisapp.config;

import ir.itsurena.zangeneh.test.testinthisapp.JWTUtil;
import ir.itsurena.zangeneh.test.testinthisapp.Service.impl.CustomUserDetailsService;
import ir.itsurena.zangeneh.test.testinthisapp.Service.exception.TokenExpiredException;
import ir.itsurena.zangeneh.test.testinthisapp.model.responses.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;


/**
 * @author Saeid Zangeneh
 *
 */

/*
We’ll use JWTAuthenticationFilter to implement a filter that -

    *reads JWT authentication token from the Authorization header of all the requests
    *validates the token
    *loads the user details associated with that token.
    *Sets the user details in Spring Security’s SecurityContext.
    *Spring Security uses the user details to perform authorization checks.
     We can also access the user details stored in the SecurityContext in our controllers to
      perform our business logic.

 */

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    private static String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String withoutBearer = getJwtFromRequest(request);

            Token token = (Token) JWTUtil.convertTokenStringToObject(withoutBearer, Token.class);

            LocalDateTime currentTime = LocalDateTime.now();
            LocalDateTime tokenTime =
                    LocalDateTime.ofInstant(Instant.ofEpochSecond(token.getExp()), TimeZone
                            .getDefault().toZoneId());

            if (currentTime.isAfter(tokenTime)) {
                throw new TokenExpiredException();
            }

            UserDetails userDetails = customUserDetailsService.loadUserByToken(withoutBearer);

            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);
    }
}