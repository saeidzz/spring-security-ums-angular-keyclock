package ir.itsurena.zangeneh.test.testinthisapp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ir.itsurena.zangeneh.test.testinthisapp.Service.exception.TokenParsingException;
import ir.itsurena.zangeneh.test.testinthisapp.Service.exception.USMResponseParseException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.IOException;
/**
 * @author Saeid Zangeneh
 *
 */
public class JWTUtil {
    public static final Logger logger = LoggerFactory.getLogger(JWTUtil.class);
    private JWTUtil(){}

    public static String decode(String jwtToken) {
        /**
         * split_string[0] :header
         * split_string[1]: payload
         * split_string[2]:verify signature
         */
        logger.info("------------ Decode JWT ------------");
        String[] split_string = jwtToken.split("\\.");
        String base64EncodedHeader = split_string[0];
        String base64EncodedBody = split_string[1];
        Base64 base64Url = new Base64(true);
 /*    String base64EncodedSignature = split_string[2];

        logger.info("~~~~~~~~~ JWT Header ~~~~~~~");

        String header = new String(base64Url.decode(base64EncodedHeader));
        logger.info("JWT Header : " + header);*/


        logger.info("~~~~~~~~~ JWT Body ~~~~~~~");
        String body = new String(base64Url.decode(base64EncodedBody));
        logger.info("JWT Body : " + body);

        return body;
    }

    public static <T>  Object convertUMSResponseToObject(String json, Class<T> objectClass) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);
        try {
            return mapper.readValue(json, objectClass);
        } catch (IOException e) {
            throw new USMResponseParseException();
        }
    }

    public static <T>  Object convertTokenStringToObject(String token, Class<T> objectClass) {
        String json=decode(token);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);
        try {
            return mapper.readValue(json, objectClass);
        } catch (IOException e) {
            throw new TokenParsingException(e.getMessage());
        }
    }

    public static  String getJwtFromHeader(String bearerToken)  {

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;

    }


}
