package ir.itsurena.zangeneh.test.testinthisapp.Service;

import ir.itsurena.zangeneh.test.testinthisapp.model.User;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
public interface UserService {
    User createUser(User user);
    User updateUser(User user);
    User finById(long id);
    List<User> finAll();
    List<User> findByOrgId(long orgId);

}
