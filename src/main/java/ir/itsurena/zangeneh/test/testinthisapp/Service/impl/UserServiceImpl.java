package ir.itsurena.zangeneh.test.testinthisapp.Service.impl;

import ir.itsurena.zangeneh.test.testinthisapp.Service.UserService;
import ir.itsurena.zangeneh.test.testinthisapp.model.User;
import ir.itsurena.zangeneh.test.testinthisapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository  userRepository;

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        User shouldBeUpdate=userRepository.findById(user.getId());
        shouldBeUpdate.name(user.getName()).firstName(user.getFirstName()).lastName(user.getLastName()).status(user.getStatus()).personalNo(user.getPersonalNo()).userName(user.getUserName());
        return userRepository.save(shouldBeUpdate);
    }

    @Override
    public User finById(long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> finAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findByOrgId(long orgId) {
        return userRepository.findByOrganization_Id(orgId);
    }
}
