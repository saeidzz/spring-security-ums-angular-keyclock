package ir.itsurena.zangeneh.test.testinthisapp.repository;

import ir.itsurena.zangeneh.test.testinthisapp.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    User save(User user);
    User findById(long id);
    List<User> findAll();
    List<User> findByOrganization_Id(long orgId);
}
