package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;
/**
 * @author Saeid Zangeneh
 *
 */
public class ClientRequestException extends RuntimeException {
    public ClientRequestException() {
        super();
    }

    public ClientRequestException(String message) {
        super(message);
    }

    public ClientRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientRequestException(Throwable cause) {
        super(cause);
    }
}
