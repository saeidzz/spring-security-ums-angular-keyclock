package ir.itsurena.zangeneh.test.testinthisapp.model.enums;
/**
 * @author Saeid Zangeneh
 *
 */
public enum UserStatus {
    ACTIVE;
}
