package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;
/**
 * @author Saeid Zangeneh
 *
 */
public class USMResponseParseException extends RuntimeException {

    public USMResponseParseException() {
        super();
    }

    public USMResponseParseException(String message) {
        super(message);
    }

    public USMResponseParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public USMResponseParseException(Throwable cause) {
        super(cause);
    }
}
