package ir.itsurena.zangeneh.test.testinthisapp.Service;

import ir.itsurena.zangeneh.test.testinthisapp.model.Organization;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
public interface OrganizationService {
    Organization createOrganization(Organization organization);
    Organization updateOrganization(Organization organization);
    Organization findById(long orgId);
    List<Organization> findAll();

}
