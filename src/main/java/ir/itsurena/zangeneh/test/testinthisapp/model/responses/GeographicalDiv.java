package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeographicalDiv {
    @JsonIgnore
    @JsonProperty("id")
    private long id;
    @JsonProperty("status")
    private Status status;
    @JsonProperty("title")
    private String title;
    @JsonProperty("type")
    private Status type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getType() {
        return type;
    }

    public void setType(Status type) {
        this.type = type;
    }
}


