package ir.itsurena.zangeneh.test.testinthisapp.Service.impl;

import ir.itsurena.zangeneh.test.testinthisapp.config.PermissionHolder;
import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UMSService umsService;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String userName) {
        return loadFromCache(userName);
    }


    private UserDetails loadFromCache(String userName) {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();


        Permission permission = PermissionHolder.getPermissionByUserName(userName);
        if (permission != null && permission.getServicePermissions() != null) {
            for (String s : permission.getServicePermissions()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(s));

            }
        }

        if (grantedAuthorities.isEmpty()) {
            throw new UsernameNotFoundException("UserName is not in our cache :" + userName);
        }
        return new User(userName, passwordEncoder.encode("ThisIsNotAValidPassWORD1"), true, true, true, true, grantedAuthorities);
    }


    public UserDetails loadUserByToken(String jwt) {
        Permission userPermission = PermissionHolder.getPermissionByJwtToken(jwt);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        userPermission.getServicePermissions().iterator().forEachRemaining(authority -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority));
        });

        return new User(userPermission.getUserName(), passwordEncoder.encode("ThisIsNotAValidPassWORD1"), true, true, true, true, grantedAuthorities);
        //  return new User("saeid",passwordEncoder.encode("ThisIsNotAValidPassWORD1"),true,true,true,true,grantedAuthorities);

    }
}