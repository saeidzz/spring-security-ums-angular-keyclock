package ir.itsurena.zangeneh.test.testinthisapp.model;

import ir.itsurena.zangeneh.test.testinthisapp.model.enums.OrganizationStatus;
import ir.itsurena.zangeneh.test.testinthisapp.model.enums.OrganizationType;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
/**
 * @author Saeid Zangeneh
 *
 */
@Entity
@Table
public class Organization {
    @Id
    private int id;
    private int code;
    private String title;
    private OrganizationType organizationType;
    private OrganizationStatus organizationStatus;
    @OneToMany
    private List<User>  userList;
    @OneToOne
    private Organization parent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Organization id(int id) {
        this.id = id;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Organization code(int code) {
        this.code = code;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Organization title(String title) {
        this.title = title;
        return this;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    public Organization organizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
        return this;
    }

    public OrganizationStatus getOrganizationStatus() {
        return organizationStatus;
    }

    public void setOrganizationStatus(OrganizationStatus organizationStatus) {
        this.organizationStatus = organizationStatus;
    }

    public Organization organizationStatus(OrganizationStatus organizationStatus) {
        this.organizationType = organizationType;
        return this;
    }

    public Organization getParent() {
        return parent;
    }

    public void setParent(Organization parent) {
        this.parent = parent;
    }

    public Organization parent(Organization parent) {
        this.parent = parent;
        return this;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public Organization userList(List<User> userList) {
        this.userList = userList;
        return this;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "id=" + id +
                ", code=" + code +
                ", title='" + title + '\'' +
                ", organizationType=" + organizationType +
                ", organizationStatus=" + organizationStatus +
                ", userList=" + userList +
                ", parent=" + parent +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return id == that.id &&
                code == that.code &&
                Objects.equals(title, that.title) &&
                organizationType == that.organizationType &&
                organizationStatus == that.organizationStatus &&
                Objects.equals(userList, that.userList) &&
                Objects.equals(parent, that.parent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, code, title, organizationType, organizationStatus, userList, parent);
    }
}
