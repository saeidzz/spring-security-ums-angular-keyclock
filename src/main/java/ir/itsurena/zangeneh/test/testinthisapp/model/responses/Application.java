package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Application {
    @JsonIgnore
    @JsonProperty("id")
    private long id;
    @JsonIgnore
    @JsonProperty("code")
    private int code;
    @JsonIgnore
    @JsonProperty("hasAccess")
    private boolean hasAccess;
    @JsonIgnore
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    @JsonProperty("ordering")
    private int ordering;
    @JsonIgnore
    @JsonProperty("prefix")
    private String prefix;
    @JsonIgnore
    @JsonProperty("status")
    private Status status;
    @JsonIgnore
    @JsonProperty("title")
    private String title;
    @JsonIgnore
    @JsonProperty("url")
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isHasAccess() {
        return hasAccess;
    }

    public void setHasAccess(boolean hasAccess) {
        this.hasAccess = hasAccess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
