package ir.itsurena.zangeneh.test.testinthisapp.repository;

import ir.itsurena.zangeneh.test.testinthisapp.model.Organization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@Repository
public interface OrganizationRepository extends CrudRepository<Organization,Long> {
    Organization save(Organization organization);
    Organization findById(long orgId);
    List<Organization> findAll();
}
