package ir.itsurena.zangeneh.test.testinthisapp.Service.impl;

import ir.itsurena.zangeneh.test.testinthisapp.Service.OrganizationService;
import ir.itsurena.zangeneh.test.testinthisapp.model.Organization;
import ir.itsurena.zangeneh.test.testinthisapp.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    OrganizationRepository organizationRepository;

    @Override
    public Organization createOrganization(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public Organization updateOrganization(Organization organization) {
        Organization shouldBeUpdate = organizationRepository.findById(organization.getId());
        shouldBeUpdate.organizationStatus(organization.getOrganizationStatus()).organizationType(organization.getOrganizationType()).code(organization.getCode()).parent(organization.getParent());
        return organizationRepository.save(shouldBeUpdate);
    }

    @Override
    public Organization findById(long orgId) {
        return organizationRepository.findById(orgId);
    }

    @Override
    public List<Organization> findAll() {
        return organizationRepository.findAll();
    }
}
