package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;

/**
 * @author Saeid Zangeneh
 *
 */

public class UMSServerException extends RuntimeException {
    public UMSServerException() {
        super();
    }

    public UMSServerException(String message) {
        super(message);
    }

    public UMSServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public UMSServerException(Throwable cause) {
        super(cause);
    }
}
