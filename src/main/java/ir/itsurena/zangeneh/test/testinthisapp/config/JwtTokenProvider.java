package ir.itsurena.zangeneh.test.testinthisapp.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Saeid Zangeneh
 *
 */

/*
The following utility class will be used for generating a JWT after
 a user logs in successfully,
and validating the JWT sent in the Authorization header of the requests
 */

// The utility class reads the JWT secret and expiration time from properties.
@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;


    public Long getUserIdFromJWT(String token) {

        return null;
    }

    public boolean validateToken(String authToken) {

        return false;
    }
}
