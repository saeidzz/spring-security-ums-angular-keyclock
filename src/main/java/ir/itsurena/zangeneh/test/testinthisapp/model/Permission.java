package ir.itsurena.zangeneh.test.testinthisapp.model;

import ir.itsurena.zangeneh.test.testinthisapp.model.responses.LoginDetail;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
/**
 * @author Saeid Zangeneh
 *
 */
public class Permission {
    private String token;
    private String userName;
    private HashSet<String> servicePermissions;
    private LoginDetail menuPermissions;
    private LocalDateTime firstRequestTime;
    private LocalDateTime expDate;

    public Permission() {
    }

    public Permission(String token, String userName, HashSet<String> servicePermissions, LoginDetail menuPermissions, LocalDateTime firstRequestTime, LocalDateTime expDate) {
        this.token = token;
        this.userName = userName;
        this.servicePermissions = servicePermissions;
        this.menuPermissions = menuPermissions;
        this.firstRequestTime = firstRequestTime;
        this.expDate = expDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public HashSet<String> getServicePermissions() {
        return servicePermissions;
    }

    public void setServicePermissions(HashSet<String> servicePermissions) {
        this.servicePermissions = servicePermissions;
    }

    public LoginDetail getMenuPermissions() {
        return menuPermissions;
    }

    public void setMenuPermissions(LoginDetail menuPermissions) {
        this.menuPermissions = menuPermissions;
    }

    public LocalDateTime getFirstRequestTime() {
        return firstRequestTime;
    }

    public void setFirstRequestTime(LocalDateTime firstRequestTime) {
        this.firstRequestTime = firstRequestTime;
    }

    public LocalDateTime getExpDate() {
        return expDate;
    }

    public void setExpDate(long expDate) {
        expDate = expDate;
    }


    @Override
    public String toString() {
        return "Permission{" +
                "token='" + token + '\'' +
                ", userName='" + userName + '\'' +
                ", servicePermissions=" + servicePermissions +
                ", menuPermissions=" + menuPermissions +
                ", firstRequestTime=" + firstRequestTime +
                ", expDate=" + expDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return Objects.equals(token, that.token) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(servicePermissions, that.servicePermissions) &&
                Objects.equals(menuPermissions, that.menuPermissions) &&
                Objects.equals(firstRequestTime, that.firstRequestTime) &&
                Objects.equals(expDate, that.expDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(token, userName, servicePermissions, menuPermissions, firstRequestTime, expDate);
    }
}
