package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;


/**
 * @author Saeid Zangeneh
 *
 */

public class TokenExpiredException extends RuntimeException {
    public TokenExpiredException() {
        super();
    }

    public TokenExpiredException(String message) {
        super(message);
    }

    public TokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenExpiredException(Throwable cause) {
        super(cause);
    }

}
