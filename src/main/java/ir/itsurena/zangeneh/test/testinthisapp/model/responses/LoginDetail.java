package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDetail {

    @JsonIgnore
    @JsonProperty("user")
    private User user;
    @JsonProperty("now")
    private long now;
    @JsonProperty("menus")
    private ArrayList<Item> menus = new ArrayList<Item>();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getNow() {
        return now;
    }

    public void setNow(long now) {
        this.now = now;
    }

    public ArrayList<Item> getMenus() {
        return menus;
    }

    public void setMenus(ArrayList<Item> menus) {
        this.menus = menus;
    }
}
