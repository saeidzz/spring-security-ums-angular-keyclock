package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Organ {
    @JsonProperty("id")
    private long id;
    @JsonProperty("code")
    private int code;
    @JsonProperty("title")
    private String title;
    @JsonProperty("Parent")
    private Organ Parent;
    @JsonProperty("OrganStatus")
    private Status OrganStatus;
    @JsonProperty("OrganType")
    private Status OrganType;
    @JsonProperty("GeographicalDiv")
    private GeographicalDiv GeographicalDiv;
    @JsonProperty("fullTitle")
    private String fullTitle;
    @JsonProperty("StatusOrgan")
    private Status StatusOrgan;
    @JsonProperty("GeoDiv")
    private GeographicalDiv GeoDiv;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Organ getParent() {
        return Parent;
    }

    public void setParent(Organ parent) {
        Parent = parent;
    }

    public Status getOrganStatus() {
        return OrganStatus;
    }

    public void setOrganStatus(Status organStatus) {
        OrganStatus = organStatus;
    }

    public Status getOrganType() {
        return OrganType;
    }

    public void setOrganType(Status organType) {
        OrganType = organType;
    }

    public ir.itsurena.zangeneh.test.testinthisapp.model.responses.GeographicalDiv getGeographicalDiv() {
        return GeographicalDiv;
    }

    public void setGeographicalDiv(ir.itsurena.zangeneh.test.testinthisapp.model.responses.GeographicalDiv geographicalDiv) {
        GeographicalDiv = geographicalDiv;
    }

    public String getFullTitle() {
        return fullTitle;
    }

    public void setFullTitle(String fullTitle) {
        this.fullTitle = fullTitle;
    }

    public Status getStatusOrgan() {
        return StatusOrgan;
    }

    public void setStatusOrgan(Status statusOrgan) {
        StatusOrgan = statusOrgan;
    }

    public ir.itsurena.zangeneh.test.testinthisapp.model.responses.GeographicalDiv getGeoDiv() {
        return GeoDiv;
    }

    public void setGeoDiv(ir.itsurena.zangeneh.test.testinthisapp.model.responses.GeographicalDiv geoDiv) {
        GeoDiv = geoDiv;
    }
}
