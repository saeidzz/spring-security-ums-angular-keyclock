package ir.itsurena.zangeneh.test.testinthisapp.model;


import ir.itsurena.zangeneh.test.testinthisapp.model.enums.UserStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;
/**
 * @author Saeid Zangeneh
 *
 */
@Entity
@Table
public class User {

    @Id
    private long id;
    private String userName;
    private String firstName;
    private String lastName;
    private UserStatus status;
    private int personalNo;
    private String name;

    @OneToOne
    private Organization organization;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public User id(long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User userName(String userName) {
        this.userName = userName;
        return this;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public User firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public User lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }


    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public User status(UserStatus status) {
        this.status = status;
        return this;
    }


    public int getPersonalNo() {
        return personalNo;
    }

    public void setPersonalNo(int personalNo) {
        this.personalNo = personalNo;
    }

    public User personalNo(int personalNo) {
        this.personalNo = personalNo;
        return this;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User name(String name) {
        this.name = name;
        return this;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public User organization(Organization organization) {
        this.organization = organization;
        return this;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", status=" + status +
                ", personalNo=" + personalNo +
                ", name='" + name + '\'' +
                ", organization=" + organization.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                personalNo == user.personalNo &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                status == user.status &&
                Objects.equals(name, user.name) &&
                Objects.equals(organization, user.organization);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userName, firstName, lastName, status, personalNo, name, organization);
    }
}
