package ir.itsurena.zangeneh.test.testinthisapp.config;

import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;

import java.util.concurrent.ConcurrentHashMap;
/**
 * @author Saeid Zangeneh
 *
 */

public class PermissionHolder {

    private PermissionHolder(){
    }

    private static ConcurrentHashMap<String, Permission> allPermissions = new ConcurrentHashMap<>();

    public static Permission addPermission(String token,Permission permission) {
        return allPermissions.put(token,permission);
    }

    public static Permission getPermissionByJwtToken(String jwtToken) {
        return allPermissions.get(jwtToken);
    }

   public static Permission getPermissionByUserName(String userName) {
        for (Permission permission1 : allPermissions.values()) {
           if (permission1.getUserName().equals(userName))
               return permission1;
       }
        return null;
    }


}
