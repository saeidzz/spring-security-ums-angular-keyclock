package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;

/**
 * @author Saeid Zangeneh
 *
 */

public class UserAuthenticationException extends RuntimeException {
    public UserAuthenticationException() {
        super();
    }

    public UserAuthenticationException(String message) {
        super(message);
    }

    public UserAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAuthenticationException(Throwable cause) {
        super(cause);
    }
}
