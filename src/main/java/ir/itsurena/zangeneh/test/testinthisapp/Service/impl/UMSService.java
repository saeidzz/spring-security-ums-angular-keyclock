package ir.itsurena.zangeneh.test.testinthisapp.Service.impl;

import ir.itsurena.zangeneh.test.testinthisapp.JWTUtil;
import ir.itsurena.zangeneh.test.testinthisapp.Service.exception.*;
import ir.itsurena.zangeneh.test.testinthisapp.model.Permission;
import ir.itsurena.zangeneh.test.testinthisapp.model.responses.LoginDetail;
import ir.itsurena.zangeneh.test.testinthisapp.model.responses.MenuResp;
import ir.itsurena.zangeneh.test.testinthisapp.model.responses.StringList;
import ir.itsurena.zangeneh.test.testinthisapp.model.responses.Token;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.TimeZone;
/**
 * @author Saeid Zangeneh
 *
 */
@Service
public class UMSService {

    private final RestTemplate restTemplate;


    public UMSService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Permission getPermission(String jwt) {
        String withoutBearer = JWTUtil.getJwtFromHeader(jwt);
        Token token = (Token) JWTUtil.convertTokenStringToObject(withoutBearer, Token.class);

        /**
         * checking token expiration
         */
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime tokenTime =
                LocalDateTime.ofInstant(Instant.ofEpochSecond(token.getExp()), TimeZone
                        .getDefault().toZoneId());

        if (currentTime.isAfter(tokenTime)) {
            throw new TokenExpiredException();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", jwt);

        String servicePermissionUrl = "http://10.40.0.210:8080/api/service/application/3/accesses";

        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> serviceResponse = restTemplate.exchange(servicePermissionUrl, HttpMethod.GET, request, String.class);
        UMSResponseValidation(serviceResponse);

        StringList servicePermissionsStringList = (StringList) JWTUtil.convertUMSResponseToObject(serviceResponse.getBody(), StringList.class);
        HashSet<String> servicePermissions = new HashSet<>(servicePermissionsStringList.getResponse());



        String menuPermissionUrl = "http://10.40.0.210:8080/api/service/login-details?applicationCode=3";

        ResponseEntity<String> menuResponse = restTemplate.exchange(menuPermissionUrl, HttpMethod.GET, request, String.class);
        UMSResponseValidation(menuResponse);

        MenuResp menuPermissions = (MenuResp) JWTUtil.convertUMSResponseToObject(menuResponse.getBody(), MenuResp.class);



        return new Permission(withoutBearer, token.getPreferredUsername(), servicePermissions, menuPermissions.getResponse(), currentTime, tokenTime);
    }

    private void UMSResponseValidation(ResponseEntity<String> response) {
        if(response.getStatusCodeValue() == 403){
            throw new UserAuthenticationException("user is not authenticated maybe there is a problem with token !!");
        }

        if(response.getStatusCodeValue() == 401){
            throw new UserAuthorizationException("user authenticated but have not authorized to access this resource");
        }

        if(response.getStatusCode().is4xxClientError()){
            throw new ClientRequestException("user sends a bad request :/");
        }

        if(response.getStatusCode().is5xxServerError()){
            throw new UMSServerException("UMS server has internal errors :| ");
        }
    }

}
