package ir.itsurena.zangeneh.test.testinthisapp.Service.exception;

/**
 * @author Saeid Zangeneh
 *
 */

public class TokenParsingException extends RuntimeException {
    public TokenParsingException() {
        super();
    }

    public TokenParsingException(String message) {
        super(message);
    }
}
