package ir.itsurena.zangeneh.test.testinthisapp.model.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Saeid Zangeneh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StringList {
    List<String> response = new ArrayList<String>();

    public List<String> getResponse() {
        return response;
    }

    public void setResponse(List<String> response) {
        this.response = response;
    }
}